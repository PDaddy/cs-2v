using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Cs2v{

public class CsvRecord{
	public static CsvRecord Read(TextReader reader, out bool eof){
		var fields = new List<CsvField>();

		while(true){
			var field = CsvField.Read(reader, out bool eor, out eof);
			if(eor){
				if(eof && fields.Count == 0)
					return null;
				return new CsvRecord(fields.ToArray());
			}
			fields.Add(field);
		}
	}

	readonly CsvField[] fields;

	public int Count => fields.Length;

	public CsvField this[int index] => index >= Count ? null : fields[index];

	public CsvRecord(params CsvField[] fields) => this.fields = fields;
	public CsvRecord(IEnumerable<CsvField> fields) => this.fields = fields as CsvField[] ?? fields.ToArray();

	public void Write(TextWriter writer){
		if(Count == 0)
			return;

		this[0].Write(writer);

		for(int i = 1; i < Count; i++){
			writer.Write(',');
			this[i].Write(writer);
		}
	}
}

}