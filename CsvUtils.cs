using System.Collections.Generic;
using System.IO;

namespace Cs2v{

public class CsvUtils{
	public static IEnumerable<CsvRecord> ReadCsvRecords(string filename) => ReadCsvRecords(File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.Read), true);
	public static IEnumerable<CsvRecord> ReadCsvRecords(Stream stream, bool dispose = false) => ReadCsvRecords(new StreamReader(stream), dispose);
	public static IEnumerable<CsvRecord> ReadCsvRecords(TextReader reader, bool dispose = false){
		try{
			while(true){
				CsvRecord record = CsvRecord.Read(reader, out bool eof);
				if(record == null)
					yield break;
				yield return record;
				if(eof)
					yield break;
			}
		}finally{
			if(dispose)
				reader?.Dispose();
		}
	}

	public static void WriteCsvRecords(string filename, IEnumerable<CsvRecord> records) => WriteCsvRecords(File.Open(filename, FileMode.Create, FileAccess.Write, FileShare.Read), records, true);
	public static void WriteCsvRecords(Stream stream, IEnumerable<CsvRecord> records, bool dispose = false) => WriteCsvRecords(new StreamWriter(stream), records, dispose);
	public static void WriteCsvRecords(TextWriter writer, IEnumerable<CsvRecord> records, bool dispose = false){
		try{
			using(var e = records.GetEnumerator()){
				if(!e.MoveNext())
					return;
				e.Current.Write(writer);
				while(e.MoveNext()){
					writer.Write("\r\n");
					e.Current.Write(writer);
				}
			}
		}finally{
			if(dispose)
				writer?.Dispose();
		}
	}
}

}