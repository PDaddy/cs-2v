using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace Cs2v{

public class CsvField{
	public static CsvField Read(TextReader reader, out bool eor, out bool eof){
		eor = eof = false;

		if((reader as StreamReader)?.EndOfStream ?? false){
			eor = eof = true;
			return null;
		}

		var txt   = new StringBuilder();
		var ws    = new StringBuilder();
		var quote = false;

		while(true){
			int c = reader.Read();
			if(c < 0){
				eor = eof = true;
				break;
			}
			if(quote){
				if(c == '"'){
					c = reader.Read();
					if(c == '"'){
						txt.Append('"');
						continue;
					}
					quote = false;
					if(c < 0){
						eor = eof = true;
						break;
					}
					goto ProcessCharacter;
				}
				txt.Append((char)c);
				continue;
			}
		ProcessCharacter:
			if(c == '"'){
				quote = true;
				continue;
			}
			if(c == ',')
				break;
			if(c == '\r'){
				c = reader.Read();
				if(c < 0){
					eor = eof = true;
					break;
				}
				if(c == '\n'){
					eor = true;
					break;
				}
				ws.Append('\r');
				goto ProcessCharacter;
			}
			if(char.IsWhiteSpace((char)c)){
				if(txt.Length > 0)
					ws.Append((char)c);
			}else{
				if(ws.Length > 0){
					txt.Append(ws);
					ws.Length = 0;
				}
				txt.Append((char)c);
			}
		}

		return new CsvField(txt.ToString());
	}

	static bool ParseBool(string s){
		switch(s?.ToLower()){
		case null:
			throw new FormatException("Cannot parse boolean value from null.");
		case "true":
		case "t":
		case "yes":
		case "y":
		case "1":
			return true;
		case "false":
		case "f":
		case "no":
		case "n":
		case "0":
			return false;
		default:
			throw new FormatException($"'{s}' is not a recognized boolean value.");
		}
	}
	static T? Parse<T>(CsvField f, Func<string, T> parse) where T : struct => f?.Value == null ? null : (T?)parse(f.Value);

	public static explicit operator string   (CsvField f) => f?.Value;
	public static explicit operator char     (CsvField f) => f.Value[0];
	public static explicit operator char?    (CsvField f) => Parse(f, s => s[0]);
	public static explicit operator bool     (CsvField f) => ParseBool(f.Value);
	public static explicit operator bool?    (CsvField f) => Parse(f, ParseBool);
	public static explicit operator byte     (CsvField f) => byte.Parse(f.Value);
	public static explicit operator byte?    (CsvField f) => Parse(f, byte.Parse);
	public static explicit operator sbyte    (CsvField f) => sbyte.Parse(f.Value);
	public static explicit operator sbyte?   (CsvField f) => Parse(f, sbyte.Parse);
	public static explicit operator short    (CsvField f) => short.Parse(f.Value);
	public static explicit operator short?   (CsvField f) => Parse(f, short.Parse);
	public static explicit operator ushort   (CsvField f) => ushort.Parse(f.Value);
	public static explicit operator ushort?  (CsvField f) => Parse(f, ushort.Parse);
	public static explicit operator int      (CsvField f) => int.Parse(f.Value);
	public static explicit operator int?     (CsvField f) => Parse(f, int.Parse);
	public static explicit operator uint     (CsvField f) => uint.Parse(f.Value);
	public static explicit operator uint?    (CsvField f) => Parse(f, uint.Parse);
	public static explicit operator long     (CsvField f) => long.Parse(f.Value);
	public static explicit operator long?    (CsvField f) => Parse(f, long.Parse);
	public static explicit operator ulong    (CsvField f) => ulong.Parse(f.Value);
	public static explicit operator ulong?   (CsvField f) => Parse(f, ulong.Parse);
	public static explicit operator float    (CsvField f) => float.Parse(f.Value);
	public static explicit operator float?   (CsvField f) => Parse(f, float.Parse);
	public static explicit operator double   (CsvField f) => double.Parse(f.Value);
	public static explicit operator double?  (CsvField f) => Parse(f, double.Parse);
	public static explicit operator decimal  (CsvField f) => decimal.Parse(f.Value);
	public static explicit operator decimal? (CsvField f) => Parse(f, decimal.Parse);
	public static explicit operator DateTime (CsvField f) => DateTime.Parse(f.Value);
	public static explicit operator DateTime?(CsvField f) => Parse(f, DateTime.Parse);
	public static explicit operator Guid     (CsvField f) => Guid.Parse(f.Value);
	public static explicit operator Guid?    (CsvField f) => Parse(f, Guid.Parse);
	public static explicit operator byte[]   (CsvField f){
		var s = f?.Value;
		if(s == null)
			return null;

		s = s.Trim();
		if(s.Length == 0)
			return new byte[0];

		var m = rxByteArray.Value.Match(f.Value);
		if(!m.Success)
			throw new FormatException($"Unexpected format for a byte array in '{s}'.");

		s = m.Groups[1].Value;
		var count = s.Length / 2;
		var rtn = new byte[count];
		for(int i = 0; i < count; i++)
			rtn[i] = (byte)(ParseHex(s[i * 2]) * 16 + ParseHex(s[i * 2 + 1]));

		return rtn;

		byte ParseHex(char c){
			if(c < '0')
				throw new FormatException($"Unexpected hex character '{c}'.");
			if(c <= '9')
				return (byte)(c - '0');
			if(c < 'A')
				throw new FormatException($"Unexpected hex character '{c}'.");
			if(c <= 'F')
				return (byte)(c - 'A' + 10);
			if(c < 'a')
				throw new FormatException($"Unexpected hex character '{c}'.");
			if(c <= 'f')
				return (byte)(c - 'a' + 10);
			throw new FormatException($"Unexpected hex character '{c}'.");
		}
	}

	public string Value { get; }

	public CsvField(string value) => Value = value;

	static readonly Lazy<Regex> rxByteArray = new Lazy<Regex>(() => new Regex(@"^(?:0x)?((?:[0-9A-Fa-f]{2})+)$", RegexOptions.Compiled));
	static readonly Lazy<Regex> rxQuotable  = new Lazy<Regex>(() => new Regex(@"^\s|\s$|[,\n""]", RegexOptions.Compiled));

	public void Write(TextWriter writer){
		if(Value == null)
			return;
		if(rxQuotable.Value.IsMatch(Value))
			writer.Write($"\"{Value.Replace("\"", "\"\"")}\"");
		writer.Write(Value);
	}
}

}