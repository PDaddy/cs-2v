# (cs)²v

This is a simple project to share some code I wrote for reading and writing CSV in C#.  It's simple and not configurable, but feature complete.

The field separator is hard-coded as a comma (`','`), and the record separator is hard-coded as a newline.  Both Unix-style newlines (`'\n'`) and DOS/Windows-style newlines (`"\r\n"`) are recognized.  Old Mac-style newlines (`'\r'`) are **not** recognized as record separators, and will instead be treated like normal white space.

Unquoted white space at the beginning or end of a field is ignored.

Standard double quotes (`'"'`) begin a quoted section of text.  A quoted section may contain white space as well as field and record separators as literal characters.  A quoted section ends with another double quote character, but two consecutive double quote characters (`"\"\""`) are treated as a single literal double quote character and don't end the quoted section.

A quoted section can be embedded within a unquoted text.  Take, for example, the following csv record:

```field one,  field two  ," field three", field four" with a quoted section "   embedded, "field ""five"" "```

This will be parsed as a record with the following fields (quoted to clearly show embedded white space):

  1. `"field one"`
  2. `"field two"`
  3. `" field three"`
  4. `"field four with a quoted section embedded"`
  5. `"field \"five\" "`